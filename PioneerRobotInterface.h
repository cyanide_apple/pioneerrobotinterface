#pragma once
#include"PioneerRobotAPI.h"
#include"RobotInterface.h"
#include"Pose.h"
#include"RangeSensor.h"
#include<iostream>
/**
* @file   PioneerRobotInterface.h
* @Author Tuçe Maide Tekeş 152120161015
* @date   Aralık, 2018
* @brief  Brief Bu sınıf RobotInterface sınıfından miras almıstır.
*
*/

//!  PioneerRobotInterface class.

class PioneerRobotInterface :public RobotInterface
{
private:
	/*!< PioneerRobotAPI türünde degiskenin adresini tutar */
	PioneerRobotAPI * robotAPI;
public:
	//! PioneerRobotInterface constructor.
	PioneerRobotInterface(PioneerRobotAPI * robotAPI, RangeSensor*sensor) :RobotInterface(sensor)
	{
		this->robotAPI = robotAPI;
		this->position = new Pose();
	}
	//! PioneerRobotInterface destructor.
	~PioneerRobotInterface();
	//! Sola donme fonksiyonudur.
	void turnLeft();
	//! Saga donme fonksiyonudur.
	void turnRight();
	//! Ileri fonksiyonudur.
	void forward(float);
	//! Ekrana bastirma fonksiyonudur.
	void print();
	//! Geri fonksiyonudur.
	void backward(float);
	//! Pose degeri dondurme fonksiyonudur.
	Pose getPose();
	//! Pose degeri set etme fonksiyonudur.
	void setPose(Pose*);
	//! Donmeyi durdurma fonksiyonudur.
	void stopTurn();
	//! Hareketi durdurma fonksiyonudur.
	void stopMove();
	//! Sersoru guncelleme fonksiyonudur.
	void updateSensors();
	//! iletisim fonksiyonudur.
	bool connect();
	//! iletisimi kesme fonksiyonudur.
	bool disconnect();

};