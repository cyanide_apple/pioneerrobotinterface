#include<iostream>
#include"PioneerRobotInterface.h"
using namespace std;


Pose PioneerRobotInterface::getPose()
{
	return *position;

}
void PioneerRobotInterface::setPose(Pose position)
{
	this->position = &position;

}
void PioneerRobotInterface::turnLeft()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
	Sleep(1000);
}
void PioneerRobotInterface::turnRight()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
	Sleep(1000);
}
void PioneerRobotInterface::forward(float speed)
{
	robotAPI->moveRobot(speed);
	Sleep(1000);
}
void PioneerRobotInterface::print()
{
	cout << "State : " << robotAPI->connect() << endl;
	cout << "Pose : " << "x : " << position->getX() << " y : " <<
		position->getY() << endl;

}
void PioneerRobotInterface::backward(float speed)
{
	robotAPI->moveRobot(speed);
	Sleep(1000);
}
void PioneerRobotInterface::stopTurn()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
}
void PioneerRobotInterface::stopMove()
{
	robotAPI->stopRobot();
}
void PioneerRobotInterface::updateSensors()
{
	robotAPI->updateRobot();
}
bool PioneerRobotInterface::connect()
{
	if (robotAPI->connect())
	{
		return true;
	}
	return false;
}
bool PioneerRobotInterface::disconnect()
{
	if (robotAPI->disconnect())
	{
		return true;
	}
	return false;
}